# Mindist
Compute the minimal distance between two meshes.

## Usage
This repository contains two scripts:
 * mindist: allows you to compute the distance between two meshes.
 * minsurf: allows you to extract a surface below some distance threshold.
 
For usage see, `python3 mindist.py --help` and `python3 minsurf.py --help`.

## Dependencies
You will need python3 and some python modules listed in requirements.txt.
Assuming you have pip, just execute the following command:
```
pip3 install --upgrade -r requirements.txt
```

You may have to modify the file `~\AppData\Local\Programs\Python\Python39\lib\sitepackages\vtkplotter\actors.py` at line 2233 to `cellData.InsertNextValue(int(inds[i]))` to fix some visualization bug. 
