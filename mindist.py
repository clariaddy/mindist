#!/usr/bin/env python3
import trimesh, argparse, os
from trimesh.proximity import closest_point as distance
from trimesh.visual import create_visual, linear_color_map
import numpy as np
from texttable import Texttable

def color(*args, **kwargs):
    color = np.array([*args], dtype=np.uint8)
    if 'n' in kwargs:
        color = np.tile(color, (kwargs['n'], 1))
    return color

def mindist(args):

    unit = args.unit
    prec = args.prec

    def vprint(*_args, **_kwargs):
        if args.verbose:
            print(*_args, **_kwargs)

    vprint("Loading mesh '{}'.  ".format(args.mesh0))
    mesh0 = trimesh.load(args.mesh0)

    vprint("Loading mesh '{}'.  ".format(args.mesh1))
    mesh1 = trimesh.load(args.mesh1)

    vprint("Computing closest points...")
    (pts, dist, tid) = distance(mesh0, mesh1.vertices)

    # sort by distances
    vprint("Extracting {} coordinate pairs...".format(args.npts))
    idx = np.argsort(dist)
    if (args.npts>0):
        idx = idx[:min(idx.size,args.npts)]
    src_pts = pts[idx]

    (dst_pts, dist, tid) = distance(mesh1, src_pts)
    idx = np.argsort(dist)
    src_pts = src_pts[idx]
    dst_pts = dst_pts[idx]
    dist    = dist[idx]

    if not args.verbose:
        sdist = np.array2string(dist, threshold=np.inf, max_line_width=np.inf, 
                    separator=' ', prefix='', suffix='', 
                    formatter={'float_kind': lambda x: '{:.{p}f}'.format(x, p=prec)})
        print(sdist[1:-1])

    # Print the points
    fmt_dist = lambda d: '{:.{p}f}'.format(d,p=prec)
    fmt_pt  = lambda pt: '({:.{p}f}, {:.{p}f}, {:.{p}f})'.format(*pt, p=prec)
    rows=[('Distance{}'.format(' ({})'.format(unit) if unit else ''),'P0','P1')]
    rows += [(fmt_dist(d),fmt_pt(p0),fmt_pt(p1)) for (d,p0,p1) in zip(dist, src_pts, dst_pts)]

    t1 = Texttable()
    t1.add_rows(rows)
    vprint('')
    vprint('Closest points')
    vprint(t1.draw())

    # Print the statistics
    vprint('''
Statistics computed for {} closest points:
  min:    {:.{p}f}{u}
  max:    {:.{p}f}{u}
  mean:   {:.{p}f}{u}
'''.format(args.npts, dist.min(), dist.max(), dist.mean(), 
        p=prec, u=unit))

    # plot in 3D view
    if args.do_plot:
        vs = args.visu_scaling
        src_pts = vs * src_pts
        dst_pts = vs * dst_pts
        mesh0.vertices[...] *= vs
        mesh1.vertices[...] *= vs

        colors = np.asarray([trimesh.visual.random_color() for _ in range(args.npts)])
        cloud0 = trimesh.points.PointCloud(src_pts, colors)
        cloud1 = trimesh.points.PointCloud(dst_pts, colors)
        entities = [trimesh.path.entities.Line(color=colors[i],
            points=(i, args.npts+i)) for i in range(args.npts)]
        lines = trimesh.path.Path3D(entities=entities, vertices=list(src_pts)+list(dst_pts))
        
        mesh0.visual = create_visual(mesh=mesh0, face_colors=color(128,128,128,64))
        mesh1.visual = create_visual(mesh=mesh1, face_colors=color(128,128,128,64))

        scene = trimesh.Scene([mesh0, mesh1, lines, cloud0, cloud1])
        scene.show()

if __name__ == '__main__':
    description='Compute minimal distance between two STL meshes.'
    epilog=''

    parser = argparse.ArgumentParser(prog='mindist',
            description=description, epilog=epilog)

    main_params = parser.add_argument_group('Main parameters')
    main_params.add_argument(type=str, dest='mesh0', help='Path to first input mesh.')
    main_params.add_argument(type=str, dest='mesh1', help='Path to second input mesh.')
    main_params.add_argument('-n', '--npoints', type=int, default=8,
            dest='npts', help='Number of closest points to extract.')
    main_params.add_argument('-u', '--unit', help='Specify distance unit for mesh.',
            dest='unit', type=str, default='')
    main_params.add_argument('-s', '--scaling', help='Rescale mesh (for vizualization only).',
            dest='visu_scaling', type=float, default=1.00)
    main_params.add_argument('-prec', '--precision', help='Specify number of digits to be printed out.',
            dest='prec', type=int, default=2)
    main_params.add_argument('-plt', '--plot', action='store_true',
            dest='do_plot', help='Plot meshes and closest points.')
    main_params.add_argument('-nv', '--no-verbose', action='store_false',
            dest='verbose', help='Disable verbosity.')
    
    args = parser.parse_args()

    for stl in (args.mesh0, args.mesh1):
        if (len(stl)<4) or (stl[-4:]!='.stl'):
            raise ValueError('Input file \'{}\' does not match input file format \'*.stl\'.'.format(stl))
        if not os.path.isfile(stl):
            raise ValueError('Input file \'{}\' does not exist.'.format(stl))

    mindist(args)
